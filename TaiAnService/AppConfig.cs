﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TaiAnService
{
    public static class AppConfig
    {
        public static bool DebugMode { get; set; } = false;
        public static string WatchDirectory { get; set; } = @"C:\PCG\CC100_in\";


        public static string DicomClientPath { get; set; } = @"C:\PCG\CC100_DICOM_out\";
        public static string DicomClient { get; set; } = @"C:\PCG\CC100_in\AdxSnapshotService\Client\AdxFODicomClient.exe";
        public static string CalledAE { get; set; } = "";
        public static string ServerIp { get; set; } = "";
        public static string ServerPort { get; set; } = "";
        public static string CallingAE { get; set; } = "";
        public static string Duration { get; set; } = "60";
        public static string Modality { get; set; } = "PCG";
        public static string CheckFileLength { get; set; } = "0";
        public static string Textout { get; set; } = "0";
        public static string TextoutPath { get; set; } = @"C:\PCG\CC100_Textout\";
        public static string FtpSend { get; set; } = "0";
        public static string FtpServer { get; set; } = "";
        public static string FtpID { get; set; } = "";
        public static string FtpPwd { get; set; } = "";
        public static string FtpPath { get; set; } = "";


        public static string PropertiesToString()
        {
            string strConfig = "AppConfig: { ";

            strConfig += "[DebugMode: " + DebugMode + "]";
            strConfig += "[WatchDirectory: " + WatchDirectory + "]";
            //strConfig += "[SnapshotDirectory: " + SnapshotDirectory + "]";
            //strConfig += "[MainframeTextDirectory: " + MainframeTextDirectory + "]";
            strConfig += "[DicomClientPath: " + DicomClientPath + "]";
            strConfig += "[CalledAE: " + CalledAE + "]";
            strConfig += "[ServerIp: " + ServerIp + "]";
            strConfig += "[ServerPort: " + ServerPort + "]";
            //strConfig += "[DoctorXmlPath: " + sDoctorXmlPath + "]";
            //strConfig += "[DoctorXmlPath: " + RenamerProgramPath + "]";
            strConfig += "[CallingAE: " + CallingAE + "]";
            strConfig += "[Duration: " + Duration + "]";
            strConfig += "[Modality: " + Modality + "]";
            strConfig += "[CheckFileLength: " + CheckFileLength + "]";
            strConfig += "[TextOut: " + Textout + "]";
            strConfig += "[TextoutPath: " + TextoutPath + "]";
            strConfig += "[FtpSend: " + FtpSend + "]";
            strConfig += "[FtpServer: " + FtpServer + "]";
            strConfig += "[FtpID: " + FtpID + "]";
            strConfig += "[FtpPwd: " + FtpPwd + "]";
            strConfig += "[FtpPath: " + FtpPath + "]";
            strConfig += " }";

            return strConfig;
        }


        //public static string[] GetTestArgs()
        //{
        //    string calledAE = @"DCTMKLOCAL";
        //    string serverIP = @"localhost";
        //    string serverPort = @"5050";
        //    //string mainframeDir = @"\\CC5\PCG":
        //    string mainframeDir = @"c:\temp\mainframetxts";
        //    //string dicomClientPath = @"C:\apps\ApoDx\AdxSnapshotService\Client\AdxFODicomClient.exe";
        //    string dicomClientPath = @"C:\dev\apodx\AdxSnapshotService\DicomClient\bin\x86\Release\AdxFODicomClient.exe";
        //    string sDoctorXmlPath = @"c:\temp\doctorxml\doctorxml.xml";
        //    string sRenamerProgrammPath = @"C:\Temp\renamerprogram\renamer.exe";
        //    string callingAE = "APODXCLIENT";

        //    string[] args = new string[]
        //    {
        //       @"/snapshotdir", @"c:\temp\snapshots\",
        //       @"/watchdir", @"c:\temp\pdfreports\",
        //       @"/mainframedir", mainframeDir,
        //       @"/dicomclientpath", dicomClientPath,
        //       @"/calledae", calledAE,
        //       @"/serverip", serverIP,
        //       @"/serverport", serverPort,
        //       @"/doctorxmlpath",sDoctorXmlPath,
        //       @"/renamerprogrampath",sRenamerProgrammPath,
        //       @"/callingae", callingAE
        //   };

        //    return args;
        //}
    }
}
