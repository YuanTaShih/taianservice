﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;


namespace TaiAnService
{
    public class Textout
    {
        public static string Textout_write(string filename, subject results)
        {
            //string configDirPath_YM = AppConfig.TextoutPath + "\\" + DateTime.Now.ToString("yyyyMM");
            //string configDirPath_YMD = configDirPath_YM + "\\" + DateTime.Now.ToString("yyyyMMdd");
            string configDirPath_YMD = AppConfig.TextoutPath;
            string filePath = configDirPath_YMD + "\\" + filename + ".txt";
            try
            {
                string contents = Textout_format(results);
                AppLog.Write(contents);
                if (!Directory.Exists(configDirPath_YMD)) Directory.CreateDirectory(configDirPath_YMD);
                File.WriteAllText(filePath, contents);
                return filePath;
            }
            catch (Exception )
            {
                AppLog.Write("Textout fail : Can not generate Textout file!");
                return "";
            }
        }

        private static string Textout_format(subject results)
        {
            string text = "";
            text = "01RSText_1 = "  +   results.HR      + Environment.NewLine +
                   "01RSText_8 = "  +   results.QRS     + Environment.NewLine +
                   "01RSText_7 = "  +   results.QTc     + Environment.NewLine +
                   "01RSText_9 = "  +   results.PR      + Environment.NewLine +
                   "01RSText_2 = "  +   results.EMAT    + Environment.NewLine +
                   "01RSText_10 = " +   results.EMATc   + Environment.NewLine +
                   "01RSText_3 = "  +   results.LVST    + Environment.NewLine +
                   "01RSText_11 = " +   results.LVSTc   + Environment.NewLine +
                   "01RSText_4 = "  +   results.S3      + Environment.NewLine +
                   "01RSText_5 = "  +   results.S4      + Environment.NewLine +
                   "01RSText_12 = " +   results.SDI     + Environment.NewLine +
                   "01RSText_13 = " +   results.Findings+ Environment.NewLine;
            
            return text;
        }
        public static int TextSendFTP(string TextOutFilePath, string FTP_IP, string ID, string PWD, string FTP_Path)
        {
            AppLog.Write(TextOutFilePath);
            string filename = TextOutFilePath.Substring(TextOutFilePath.LastIndexOf("\\") + 1, TextOutFilePath.Length - TextOutFilePath.LastIndexOf("\\") - 1);
            
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send(FTP_IP, 500);
                if (reply.Status.ToString() == "Success")
                    AppLog.Write("Status :  " + reply.Status + ",  Time : " + reply.RoundtripTime.ToString() + ", FTP Address : " + reply.Address);
                else
                {
                    AppLog.Write("ERROR: FTP server is offline!!");
                    return 0;
                }
            }
            catch
            {
                AppLog.Write("ERROR: FTP server offline!!");
                //AppLog.Write("--------------------------------Finished-------------------------");
                return 0;
            }

            try
            {
                string CreateFTP = "";
                // Copy the contents of the file to the request stream.
                if (FTP_Path == "NULL")
                    CreateFTP = "ftp://" + FTP_IP + "/" + filename;
                else
                    CreateFTP = "ftp://" + FTP_IP + "/" + FTP_Path + "/" + filename;
                AppLog.Write(CreateFTP);

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(CreateFTP);
                AppLog.Write("filename: " + filename );
                request.Credentials = new NetworkCredential(ID, PWD);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                StreamReader sourceStream = new StreamReader(TextOutFilePath);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                AppLog.Write("Upload " + TextOutFilePath + "File Complete, status： " + response.StatusDescription);
                response.Close();

                string filePath = "";
                try
                {
                    // move file to sent folder
                    string configDirPath = AppConfig.TextoutPath + "\\sent\\";
                    filePath = configDirPath + filename;
                    if (!Directory.Exists(configDirPath)) Directory.CreateDirectory(configDirPath);
                    if (File.Exists(filePath)) File.Delete(filePath);
                    File.Move(TextOutFilePath, filePath);
                    AppLog.Write("Move " + TextOutFilePath + " to " + filePath);
                }
                catch
                {
                    AppLog.Write("ERROR: Move " + TextOutFilePath + " to " + filePath +  "failed!");
                }

                return 1;
            }
            catch
            {
                AppLog.Write("ERROR: " + TextOutFilePath + " transmit to FTP server failed!");
                return 0;
            }

        }
    }
}
