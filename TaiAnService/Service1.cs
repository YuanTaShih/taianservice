﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Drawing;
using System.Threading;
using System.Net.NetworkInformation;

namespace TaiAnService
{
    public partial class Service1 : ServiceBase
    {
        private System.Timers.Timer timer;
        private double timer_interval = 60000; 

        public static string filename = "";
        public static string pathname = "";
        public static string DicomFileName = "";

        public static DirectoryInfo dirInfo;
        public static FileSystemWatcher _watch = new FileSystemWatcher();


        public Service1()
        {
            

            InitializeComponent();
            timer_interval = Convert.ToDouble(AppConfig.Duration) * 1000;
           

            AppLog.Write("Service initialized! Version: 1.4.1");
            AppLog.Write("Watch folder: " + AppConfig.WatchDirectory);
            AppLog.Write("DICOM Client folder: " + AppConfig.DicomClientPath);
            AppLog.Write("DICOM Client exe: " + AppConfig.DicomClient);
            AppLog.Write("DICOM AETitle: " + AppConfig.CalledAE);
            AppLog.Write("DICOM Server IP: " + AppConfig.ServerIp);
            AppLog.Write("DICOM Server port: " + AppConfig.ServerPort);
            AppLog.Write("DICOM Called AE: " + AppConfig.CalledAE);
            AppLog.Write("DICOM Calling AE: " + AppConfig.CallingAE);
            AppLog.Write("Timer interval: " + (timer_interval/1000).ToString() + "s");
            AppLog.Write("Modality: " + AppConfig.Modality);
            AppLog.Write("CheckFileLength: " + AppConfig.CheckFileLength);
            AppLog.Write("Textout: " + AppConfig.Textout);
            if (AppConfig.Textout == "1")
            {
                AppLog.Write("Textout Path: " + AppConfig.TextoutPath);
                AppLog.Write("FTP Send: " + AppConfig.FtpSend);
                if (AppConfig.FtpSend == "1")
                {
                    AppLog.Write("FTP Server: " + AppConfig.FtpServer);
                    AppLog.Write("FTP ID: " + AppConfig.FtpID);
                    AppLog.Write("FTP Pwd: " + AppConfig.FtpPwd);
                    AppLog.Write("FTP Path: " + AppConfig.FtpPath);
                }
            }
            
            AppLog.Write("------------------------------------------------------------");

            if (!Directory.Exists(AppConfig.DicomClientPath)) Directory.CreateDirectory(AppConfig.DicomClientPath);

            AppConfig.DebugMode = false;
            //firstCheck();
            this.timer = new System.Timers.Timer();
            this.timer.Interval = timer_interval;
            this.timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

        }

        protected override void OnStart(string[] args)
        {
            AppLog.Write("SERVICE STARTED.");
            this.timer.Enabled = true;
            this.timer.Start(); //啟動timer

            //_watch.Path = AppConfig.WatchDirectory; ;
            ////設定所要監控的檔案
            //_watch.Filter = "*.pdf";
            ////設定是否監控子資料夾
            //_watch.IncludeSubdirectories = true;
            ////設定是否啟動元件，此部分必須要設定為 true，不然事件是不會被觸發的
            //_watch.EnableRaisingEvents = true;
            //////設定觸發事件
            //_watch.Created += new FileSystemEventHandler(_watch_Created);
            //_watch.Renamed += new RenamedEventHandler(_watch_Created);
        }

        protected override void OnStop()
        {
            AppLog.Write("SERVICE Stopped.");
            this.timer.Enabled = false;
            this.timer.Stop();
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.timer.Stop();
            mainFunction();
            this.timer.Start();
        }

        //---------------------------------------------------------------------//
        //                     Main Function                                   //
        //---------------------------------------------------------------------//
        private void mainFunction()
        {
            AppConfig.DebugMode = true;
            firstCheck();
        }

        private static void firstCheck()
        {

            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send(AppConfig.ServerIp, 500);
                if (reply.Status.ToString() == "Success")
                {
                    AppLog.Write("Status :  " + reply.Status + ",  Time : " + reply.RoundtripTime.ToString() + ", Dicom Server Address : " + reply.Address);
                    //Console.WriteLine(reply.ToString());
                    //AppLog.Write("---------------------------------------------------------");

                }
                else
                {
                    AppLog.Write("ERROR: Dicom server is offline!!");
                    //AppLog.Write("--------------------------------Finished-------------------------");
                    return;
                }

            }
            catch
            {
                AppLog.Write("ERROR: Dicom server offline!!");
                //AppLog.Write("--------------------------------Finished-------------------------");
                return;
            }




            try
            {
                List<string> dirs = new List<string>(Directory.EnumerateFiles(AppConfig.WatchDirectory, "*.pdf"));
                for (int i = 0; i < dirs.Count; i++)
                {
                    pathname = dirs[i];
                    filename = pathname.Substring(pathname.LastIndexOf("\\") + 1);
                    AppLog.Write("Get File: " + filename);


                    string dcmFilePath = PDF2DICOM(pathname); 
                    if(dcmFilePath.Length != 0)
                    {
                        AppLog.Write("Dicom File: " + dcmFilePath);
                        if (dcmFilePath == "")
                        //if (!File.Exists(dcmFilePath))
                        {
                            AppLog.Write("Can not convert to DICOM: " + pathname);
                            ErrorProcessingSnapshot(dcmFilePath, "DICOM file not found: " + dcmFilePath);
                            return;
                        }
                        else
                        {
                            AppLog.Write("DICOM launch: " + dcmFilePath);
                            LaunchDicomClient(AppConfig.CalledAE, AppConfig.ServerIp, AppConfig.ServerPort, dcmFilePath);

                            if (File.Exists(AppConfig.DicomClientPath + "\\" + filename)) File.Delete(AppConfig.DicomClientPath + "\\" + filename);
                            File.Move(pathname, AppConfig.DicomClientPath + "\\" + filename);
                            AppLog.Write("Move " + pathname + " to " + AppConfig.DicomClientPath + "\\" + filename);
                            DataTools.SetCounter(filename);
                        }
                    }
                    else
                    {
                        string ErrorPath = AppConfig.WatchDirectory + "\\ErrorPDF\\";
                        AppLog.Write("Move Error PDF " + pathname + " to " + ErrorPath + filename);
                        if (!Directory.Exists(ErrorPath)) Directory.CreateDirectory(ErrorPath);
                        if (File.Exists(ErrorPath + filename)) File.Delete(ErrorPath + filename);
                        File.Move(pathname, ErrorPath + filename);
                    }
                    AppLog.Write("--------------------------------Finished-------------------------");
                }
                
            }
            catch (UnauthorizedAccessException UAEx)
            {
                Console.WriteLine(UAEx.Message);
            }
            catch (PathTooLongException PathEx)
            {
                Console.WriteLine(PathEx.Message);
            }
        }

        private static string PDF2DICOM(string filename)
        {

            AppLog.Write("check the length of filename with .pdf is " + AppConfig.CheckFileLength);
            int fLength = filename.Substring(filename.LastIndexOf("\\") + 1).Length;  //check the length of filename with .pdf is checkfilelength
            if (Convert.ToInt32(AppConfig.CheckFileLength) == fLength || Convert.ToInt32(AppConfig.CheckFileLength) == 0)  
            {
                AppLog.Write("the length of filename with .pdf is " + fLength.ToString());
                int year = 2017;
                int month = 2;
                int day = 21;
                int hour = 15;
                int minute = 00;
                int second = 00;
                

                subject subject1 = cc100_info(filename);

                if (subject1.name == "FAIL")
                    return "";

                Patient patient = new Patient();
                patient.ID = subject1.ID;
                patient.FirstName = subject1.name;
                //patient.LastName = subject1.name;

                if (subject1.gender == "Male") patient.Gender = "M";
                else if (subject1.gender == "Female") patient.Gender = "F";
                else patient.Gender = subject1.gender;

                if (subject1.DOB != "")
                {
                    var date = DateTime.Parse(subject1.DOB);
                    patient.DateOfBirth = string.Format("{0}{1}{2}", date.Year, date.Month, date.Day);
                    AppLog.Write("change DOB format from " + subject1.DOB + " to " + patient.DateOfBirth);
                    //patient.DateOfBirth = subject1.DOB;
                }
                else
                    patient.DateOfBirth = subject1.DOB;

                if(subject1.date != "")
                {
                    var date = DateTime.Parse(subject1.date);
                    AppLog.Write("Test date: " + date.ToString());
                    year = date.Year;
                    month = date.Month;
                    day = date.Day;
                    hour = date.Hour;
                    minute = date.Minute;
                    second = date.Second;
                }
                DateTime snapshotDateTime = new DateTime(year, month, day, hour, minute, second);                
                string sReqNum = filename.Substring(filename.LastIndexOf("\\") + 1, fLength - 4);
                AppLog.Write("Read parameters done");

                /* ************************************************************************************************ */
                // Process Texout --> FTP Server
                /* ************************************************************************************************ */

                if (AppConfig.Textout == "1")
                {   
                    string textpath = Textout.Textout_write(sReqNum, subject1);
                    int r1 = 0;

                    if (AppConfig.FtpSend == "1")
                        if (textpath != "")
                            r1 = Textout.TextSendFTP(textpath, AppConfig.FtpServer, AppConfig.FtpID, AppConfig.FtpPwd, AppConfig.FtpPath);     
                }


                string pdfFilePath = filename;
                /* ************************************************************************************************ */
                // Process PDF --> JPEG --> DICOM file
                /* ************************************************************************************************ */
                string dcmFilePath = "";
                try
                {
                    AppLog.Write("Attempting to convert PDF to image.");
                    Bitmap bmpImg = DataTools.PdfToImage(pdfFilePath);
                    AppLog.Write("Attempting to create Dicom file.");
                    dcmFilePath = DataTools.CreateDicomFile(bmpImg, patient, snapshotDateTime, sReqNum);
                    return dcmFilePath;
                }
                catch (Exception ex)
                {
                    ErrorProcessingSnapshot(pdfFilePath, "Error converting PDF to DICOM file. " + ex.Message);
                    //return "";
                    return "";
                }
            }
            else
            {
                AppLog.Write("Length of " + filename + " is not match " + AppConfig.CheckFileLength);
                return "";
            }

        }

        private static void LaunchDicomClient(string calledAE, string serverIp, string serverPort, string dicomFilePath)
        {
            string clientArgs = " /logdir " + AppLog.LogFileDirPath
               + " /calledae " + AppConfig.CalledAE
               + " /serverip " + AppConfig.ServerIp
               + " /serverport " + AppConfig.ServerPort
               + " /dicomfile " + dicomFilePath
               + " /callingae " + AppConfig.CallingAE
               + " /modality " + AppConfig.Modality;

            AppLog.Write("Calling dicom client with args:" + clientArgs);

            try
            {
                Process clientProc = new Process();
                clientProc.StartInfo.FileName = AppConfig.DicomClient;         
                clientProc.StartInfo.Arguments = clientArgs;
                

                AppLog.Write("Client process start() returned: " + clientProc.Start().ToString());
            }
            catch (Exception ex)
            {
                string errorMessage = "Error launching DICOM client application. | EXE path: " + AppConfig.DicomClientPath + " | " + ex.Message;
                ErrorProcessingSnapshot(dicomFilePath, errorMessage);
            }
        }

        private static subject cc100_info(string file)
        {
            AppLog.Write("Enter Read CC100 parameters");
            subject s1 = new subject();
            AppLog.Write(file);
            PdfReader pdfReader = new PdfReader(file); 

            //AppLog.Write("Thread sleep 10000ms!");
            //Thread.Sleep(10000);
            string output = "";
            try
            {
                
                StringWriter output_text = new StringWriter();
                output_text.WriteLine(PdfTextExtractor.GetTextFromPage(pdfReader, 1, new SimpleTextExtractionStrategy()));
                output = output_text.ToString();
                //AppLog.Write(output);
                
                // find out name ID DOB gender time
                int temp1 = output.IndexOf("Name:") + "Name:".Length;
                int temp2 = output.IndexOf("\n", temp1);
                s1.name = output.Substring(temp1, temp2 - temp1);
                if (s1.name.Length == 0) s1.name = "Empty";
                else s1.name = s1.name.Substring(1,s1.name.Length-1);
                AppLog.Write(s1.name);
            

                temp1 = output.IndexOf("Patient ID:") + "Patient ID:".Length;
                temp2 = output.IndexOf("\n", temp1);
                s1.ID = output.Substring(temp1, temp2 - temp1);
                if (s1.ID.Length == 0) s1.ID = "Empty";
                else s1.ID = s1.ID.Substring(1, s1.ID.Length - 1);
                AppLog.Write(s1.ID);

                temp1 = output.IndexOf("Gender:") + "Gender:".Length;
                temp2 = output.IndexOf("\n", temp1);
                s1.gender = output.Substring(temp1, temp2 - temp1);
                if (s1.gender.Length == 0) s1.gender = "E";
                else s1.gender = s1.gender.Substring(1, s1.gender.Length - 1);
                AppLog.Write(s1.gender);

                temp1 = output.IndexOf("Test time:") + "Test time:".Length;
                temp2 = output.IndexOf("\n", temp1);
                s1.date = output.Substring(temp1, temp2 - temp1);

                temp1 = output.IndexOf("Date of birth:") + "Date of birth:".Length;
                temp2 = output.IndexOf("\n", temp1);
                s1.DOB = output.Substring(temp1, temp2 - temp1);
            
                if(AppConfig.Textout == "1")
                {
                    temp1 = output.IndexOf("HR: ") + "HR: ".Length;
                    temp2 = output.IndexOf(" ", temp1);
                    try { s1.HR = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.HR = "n/a"; }

                    temp1 = output.IndexOf("QRS dur: ") + "QRS dur: ".Length;
                    temp2 = output.IndexOf(" ", temp1);
                    try { s1.QRS = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.QRS = "n/a"; }

                    temp1 = output.IndexOf("PR int: ") + "PR int: ".Length;
                    temp2 = output.IndexOf(" ", temp1);
                    try { s1.PR = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.PR = "n/a"; }

                    temp1 = output.IndexOf("QTc: ") + "QTc: ".Length;
                    temp2 = output.IndexOf(" ", temp1);
                    try { s1.QTc = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.QTc = "n/a"; }

                    temp1 = output.IndexOf("EMAT(c): ") + "EMAT(c): ".Length;
                    temp2 = output.IndexOf(" ", temp1);
                    try
                    {
                        s1.EMAT = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString();
                        int temp3 = output.IndexOf(" (", temp2) + " (".Length;
                        int temp4 = output.IndexOf("%)", temp3);
                        string PPP = output.Substring(temp4, temp4 - temp3);
                        try { s1.EMATc = Convert.ToDouble(output.Substring(temp3, temp4 - temp3)).ToString(); } catch { s1.EMATc = "n/a"; };
                    }
                    catch { s1.EMAT = "n/a"; s1.EMATc = "n/a"; }

                    temp1 = output.IndexOf("LVST(c): ") + "LVST(c): ".Length;
                    temp2 = output.IndexOf(" ", temp1);
                    try
                    {
                        s1.LVST = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString();
                        int temp3 = output.IndexOf(" (", temp2) + " (".Length;
                        int temp4 = output.IndexOf("%)", temp3);
                        try { s1.LVSTc = Convert.ToDouble(output.Substring(temp3, temp4 - temp3)).ToString(); } catch { s1.LVSTc = "n/a"; }
                    }
                    catch { s1.LVST = "n/a"; s1.LVSTc = "n/a"; }

                    temp1 = output.IndexOf("S3: ") + "S3: ".Length;
                    temp2 = output.IndexOf("\n", temp1);
                    try { s1.S3 = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.S3 = "n/a"; }

                    temp1 = output.IndexOf("S4: ") + "S4: ".Length;
                    temp2 = output.IndexOf("\n", temp1);
                    try { s1.S4 = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.S4 = "n/a"; }

                    temp1 = output.IndexOf("SDI: ") + "SDI: ".Length;
                    temp2 = output.IndexOf("\n", temp1);
                    try { s1.SDI = Convert.ToDouble(output.Substring(temp1, temp2 - temp1)).ToString(); } catch { s1.SDI = "n/a"; }

                    temp1 = output.IndexOf("Findings: ") + "Findings: ".Length;
                    temp2 = output.IndexOf("Notes:", temp1);
                    try { s1.Findings = output.Substring(temp1, temp2 - temp1); } catch { s1.Findings = "n/a"; }

                    s1.Findings = s1.Findings.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " "); 

                }
            }
            catch (Exception e)
            {
                AppLog.Write(e.ToString());
            }
            pdfReader.Dispose();
            //AppLog.Write("Exception out!");

            return s1;
        }

        private static void ErrorProcessingSnapshot(string filePath, string errorMessage = "")
        {
            AppLog.Write(errorMessage);
            string dateTime = DateTime.Now.ToString("yyyyMMdd HH:ss:mm.fff");

            try
            {
                string failFilePath = AppConfig.WatchDirectory + "\\ERROR_" + System.IO.Path.GetFileNameWithoutExtension(filePath) + ".txt";
                File.WriteAllText(failFilePath, dateTime + " " + errorMessage);
            }
            catch (Exception exCreateFile) { AppLog.Write("Error creating fail text file. " + exCreateFile.Message); }
        }

    }
}
