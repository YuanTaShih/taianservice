﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TaiAnService
{
    public class AppLog
    {
        public static string LogFileDirPath { get; set; } = @"C:\PCG\logs";
        private static string LogName { get; set; } = @"snapshot_service_log_";

        public static void Write(string info)
        {
            string logFileNameToday = LogFileDirPath + "\\" + LogName + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            string logInfo = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " " + info;

            try
            {
                if (!Directory.Exists(LogFileDirPath)) Directory.CreateDirectory(LogFileDirPath);
                File.AppendAllText(logFileNameToday, logInfo + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: can not access log file " + logFileNameToday + ".\n" + ex.Message);
            }
        }
    }
}
