﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using O2S.Components.PDFRender4NET;
using Dicom.Data;
using System.IO;
using System.Diagnostics;

namespace TaiAnService
{
    public class subject
    {
        public string name = "FAIL";
        public string gender;
        public string DOB;
        public string date;
        public string ID;
        public int age;
        public string HR;
        public string QRS;
        public string PR;
        public string QTc;
        public string EMAT;
        public string LVST;
        public string S3;
        public string S4;
        public string SDI;
        public string EMATc = "";
        public string LVSTc = "";
        public string Findings = "";
    }

    public static class DataTools
    {
        public static string CreateDicomFile(Bitmap bmpImg, Patient patient, DateTime snapshotDateTime, string reqNum)
        {
            AppLog.Write("CreateDicomFile() Args: " + patient.ToString() + " " + snapshotDateTime.ToString("yyyy-M-d-H-m-s") + " " + reqNum);

            string strDateTime = DateTime.Now.ToString("yyyyMMdd_hhmmss_ff_");
            //string dcmFilePath = AppConfig.WatchDirectory + "\\temp\\" + strDateTime + patient.ID + "_" + reqNum + ".dcm";
            string dcmFilePath = AppConfig.DicomClientPath +"\\" + reqNum +"_"+ strDateTime + patient.ID  + ".dcm";
            string patientDOB = ""; //yyyyMMdd
            string patientGender = "";
            if (patient.DateOfBirth != null) patientDOB = patient.DateOfBirth;
            //if (patient.Gender != null && patientGender.Length >= 1) patientGender = patient.Gender.Substring(0, 1).ToUpper();
            if (patient.Gender != null) patientGender = patient.Gender;

            try
            {
                CreateDicomImageFile(
                    bmpImg,
                    dcmFilePath,
                    snapshotDateTime,
                    "" + patient.ID,
                    "" + reqNum,
                    patientDOB,
                    patientGender,
                    patient.FirstName
                );
            }
            catch (Exception ex)
            {
                AppLog.Write("Error creating DICOM file.  " + ex.Message);
                throw;
            }

            return dcmFilePath;
        }

        public static DicomFileFormat CreateDicomImageFile(Bitmap bmpImg, string outputFilePath,
            DateTime dateTime, string patientId, string reqNumber, string patientDOB, string patientGender, string patientName)
        {
            AppLog.Write("Create DICOM file parameters: "
                + bmpImg.Size.Width + " "
                + bmpImg.Size.Height + " "
                + outputFilePath + " "
                + dateTime.ToString("yyyyMMdd HH:mm:ss.fff") + " "
                + patientId + " "
                + reqNumber + " "
                + patientDOB + " "
                + patientGender + " "
                + patientName);

            //Create the dataset and set attribute elements
            DcmDataset ds = new DcmDataset(DicomTransferSyntax.ExplicitVRLittleEndian);

            //SOP Common module attributes
            ds.AddElementWithValue(DicomTags.SOPClassUID, DicomUID.SecondaryCaptureImageStorage);
            ds.AddElementWithValue(DicomTags.SOPInstanceUID, DicomUID.Generate());

            //Patient module attributes
            ds.AddElementWithValueString(DicomTags.PatientID, patientId);
            ds.AddElementWithValueString(DicomTags.PatientsName, patientName);
            ds.AddElementWithValueString(DicomTags.PatientsBirthDate, patientDOB); //YYYYMMDD
            ds.AddElementWithValueString(DicomTags.PatientsSex, patientGender);

            //General Study module attributes
            ds.AddElementWithValue(DicomTags.StudyInstanceUID, DicomUID.Generate());
            ds.AddElementWithValueString(DicomTags.StudyDate, dateTime.ToString("yyyyMMdd")); //YYYYMMDD
            ds.AddElementWithValueString(DicomTags.StudyTime, dateTime.ToString("HHmmss.fff")); //HHMMSS.mmm
            ds.AddElementWithValueString(DicomTags.ReferringPhysiciansName, "AUDICOR PHYSICIAN");
            ds.AddElementWithValueString(DicomTags.StudyID, "1");
            ds.AddElementWithValueString(DicomTags.AccessionNumber, reqNumber);

            //General Series module attributes
            ds.AddElementWithValue(DicomTags.Modality, AppConfig.Modality);
            ds.AddElementWithValue(DicomTags.SeriesInstanceUID, DicomUID.Generate());
            ds.AddElementWithValue(DicomTags.SeriesNumber, 1);

            ds.AddElementWithValueString(DicomTags.StationName, "P.C.G. AUDICOR");
            ds.AddElementWithValueString(DicomTags.OperatorsName, "P.C.G. AUDICOR OPERATOR");
            ds.AddElementWithValue(DicomTags.InstanceNumber, 1);

            //Encapsulated document module attributes (on pp12-14 of the standard supplement)
            //ftp://medical.nema.org/medical/dicom/final/sup104_ft.pdf
            ds.AddElementWithValue(DicomTags.ContentDate, dateTime.ToString("yyyyMMdd"));
            ds.AddElementWithValue(DicomTags.ContentTime, dateTime.ToString("HHmmss.fff"));
            ds.AddElementWithValue(DicomTags.AcquisitionDateTime, dateTime.ToString("yyyyMMddHHmmss.ffffff"));
            ds.AddElementWithValueString(DicomTags.DocumentTitle, "Phonocardiogram");

            //Image attributes
            byte[] pixels = GetPixelRgbBytes(bmpImg);

            DcmPixelData pixelData = new DcmPixelData(ds);
            pixelData.AddFrame(pixels);
            pixelData.IsLossy = true;
            pixelData.LossyCompressionMethod = "ISO_10918_1";
            pixelData.UpdateDataset(ds);

            ds.Remove(DicomTags.LossyImageCompressionRatio);
            ds.AddElementWithValue(DicomTags.Rows, (ushort)bmpImg.Size.Height);
            ds.AddElementWithValue(DicomTags.Columns, (ushort)bmpImg.Size.Width);
            ds.AddElementWithValue(DicomTags.BitsAllocated, (ushort)8);
            ds.AddElementWithValue(DicomTags.BitsStored, (ushort)8);
            ds.AddElementWithValue(DicomTags.HighBit, (ushort)7);
            ds.AddElementWithValue(DicomTags.SamplesPerPixel, (ushort)3);
            ds.AddElementWithValue(DicomTags.PhotometricInterpretation, "RGB");
            ds.AddElementWithValue(DicomTags.PixelRepresentation, (ushort)0);
            ds.AddElementWithValue(DicomTags.PlanarConfiguration, (ushort)0);

            AppLog.Write("Saving to file: " + outputFilePath);
            DicomFileFormat ff = new DicomFileFormat(ds);

            try
            {
                ff.Save(outputFilePath, Dicom.DicomWriteOptions.Default);
            }
            catch (Exception ex)
            {
                AppLog.Write("Error saving DICOM file.  " + ex.Message);
            }

            return ff;
        }

        public static Bitmap PdfToImage(string pdfFilePath)
        {
            PDFFile pdfFile = PDFFile.Open(pdfFilePath);
            int definition = 3; //1~10

            // start to convert each page
            Bitmap bmpImg = pdfFile.GetPageImage(0, 56 * definition); //56
            //bmpImg = rotateImage90(bmpImg);
            bmpImg.RotateFlip(RotateFlipType.Rotate90FlipNone);
            

            pdfFile.Dispose();
            return bmpImg;
        }

        private static Bitmap rotateImage90(Bitmap b)
        {
            Bitmap returnBitmap = new Bitmap(b.Height, b.Width);
            Graphics g = Graphics.FromImage(returnBitmap);
            g.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
            g.RotateTransform(90);
            g.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
            g.DrawImage(b, new Point(0, 0));
            return returnBitmap;
        }

        private static byte[] GetPixelRgbBytes(Bitmap img)
        {
            List<byte> rgbValues = new List<byte>();

            AppLog.Write("img format, width, height: " + img.PixelFormat + " " + img.Width + " " + img.Height + " ");

            for (int i = 0; i < img.Height; i++)
            {
                for (int j = 0; j < img.Width; j++)
                {
                    Color pixel = img.GetPixel(j, i);
                    rgbValues.Add(pixel.R);
                    rgbValues.Add(pixel.G);
                    rgbValues.Add(pixel.B);
                }
            }

            return rgbValues.ToArray();
        }

        public static void SetCounter(string file)
        {
            string logInfo = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + ": " + file;

            string appDataDir = Path.GetFullPath(Process.GetCurrentProcess().MainModule.FileName);
            string configDirPath = "C:\\Windows\\ApoDx\\" + DateTime.Now.ToString("yyyy");
            string counterfile = configDirPath +"\\" + DateTime.Now.ToString("yyyyMM") + ".txt";
            try
            {
                //AppLog.Write(configDirPath);
                if (!Directory.Exists(configDirPath)) Directory.CreateDirectory(configDirPath);
                File.AppendAllText(counterfile, logInfo + Environment.NewLine);
            }
            catch
            {
                return;
            }

        }

    }
}
