﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaiAnService
{
    /* Example data
    <Patient>
        <Id>4129633-3</Id>
        <Gender>Male</Gender>
        <FirstName />
        <LastName />
        <DateOfBirth>1981-05-26T00:00:00</DateOfBirth>
    </Patient>
    */
    public class Patient
    {
        //[XmlElement("Id")]
        //public string ID { get; set; }
        //[XmlElement("Gender")]
        //public string Gender { get; set; }
        //[XmlElement("FirstName")]
        //public string FirstName { get; set; }
        //[XmlElement("LastName")]
        //public string LastName { get; set; }
        //[XmlElement("DateOfBirth")]
        //public string DateOfBirth { get; set; }

        //public override string ToString()
        //{
        //    string str = "";
        //    foreach (PropertyInfo p in this.GetType().GetProperties())
        //        str += p.Name + " " + p.GetValue(this) + " ";
        //    return str;
        //}

        private string id = "";
        private string gender = "";
        private string firstName = "";
        private string lastName = "";
        private string BOD = "";
        private string TestingDate = "";
        private int age = 0;
        private string hr = "";
        private string qrs = "";
        private string pr = "";
        private string qtc = "";
        private string emat = "";
        private string ematc = "";
        private string lvst = "";
        private string lvstc = "";
        private string s3 = "";
        private string s4 = "";
        private string sdi = "";
        private string findings = "";

        public string ID
        {
            get {return id;}
            set {id = value;}
        }
        public string Gender
        {
            get{return gender;}
            set{gender = value;}
        }
        public string FirstName
        {
            get{return firstName;}
            set{firstName = value;}
        }
        public string LastName
        {
            get{return lastName;}
            set{lastName = value;}
        }
        public string DateOfBirth
        {
            get{return BOD;}
            set{BOD = value;}
        }
        public string ExpDate
        {
            get{return TestingDate;}
            set{TestingDate = value;}
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        public string HR
        {
            get { return hr; }
            set { hr = value; }
        }
        public string QRS
        {
            get { return qrs; }
            set { qrs = value; }
        }
        public string PR
        {
            get { return pr; }
            set { pr = value; }
        }
        public string QTc
        {
            get { return qtc; }
            set { qtc = value; }
        }
        public string EMAT
        {
            get { return emat; }
            set { emat = value; }
        }
        public string EMATc
        {
            get { return ematc; }
            set { ematc = value; }
        }
        public string LVST
        {
            get { return lvst; }
            set { lvst = value; }
        }
        public string LVSTc
        {
            get { return lvstc; }
            set { lvstc = value; }
        }
        public string S3
        {
            get { return s3; }
            set { s3 = value; }
        }
        public string S4
        {
            get { return s4; }
            set { s4 = value; }
        }
        public string SDI
        {
            get { return sdi; }
            set { sdi = value; }
        }
        public string Findings
        {
            get { return findings; }
            set { findings = value; }
        }
    }
}
