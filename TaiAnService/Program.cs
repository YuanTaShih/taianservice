﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TaiAnService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            //*** IMPORTANT!!! Set DebugMode = false for live deployment ***
            AppConfig.DebugMode = true;

            CmdLineArgs cmdArgs = new CmdLineArgs(args);

            //AppConfig.SnapshotDirectory = cmdArgs.GetArg("/snapshotdir");
            AppConfig.WatchDirectory = cmdArgs.GetArg("/watchdir");
            AppConfig.DicomClientPath = cmdArgs.GetArg("/dicomclientpath");
            //AppConfig.MainframeTextDirectory = cmdArgs.GetArg("/mainframedir");

            AppConfig.CalledAE = cmdArgs.GetArg("/calledae");
            AppConfig.ServerIp = cmdArgs.GetArg("/serverip");
            AppConfig.ServerPort = cmdArgs.GetArg("/serverport");
            AppConfig.DicomClient = cmdArgs.GetArg("/dicomclientexe");
            AppConfig.CallingAE = cmdArgs.GetArg("/callingae");
            AppConfig.Duration = cmdArgs.GetArg("/duration");
            AppConfig.Modality = cmdArgs.GetArg("/modality");
            AppConfig.CheckFileLength = cmdArgs.GetArg("/checkfilelength");
            AppConfig.Textout = cmdArgs.GetArg("/textout");
            AppConfig.TextoutPath = cmdArgs.GetArg("/textoutpath");
            AppConfig.FtpSend = cmdArgs.GetArg("/ftpsend");
            AppConfig.FtpServer = cmdArgs.GetArg("/ftpserver");
            AppConfig.FtpID = cmdArgs.GetArg("/ftpid");
            AppConfig.FtpPwd = cmdArgs.GetArg("/ftppwd");
            AppConfig.FtpPath = cmdArgs.GetArg("/ftppath");



            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
